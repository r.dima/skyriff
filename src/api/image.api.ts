import httpClient from "./httpClient";

class Image {
  getImages(ids = []) {
    return httpClient({
      method: "get",
      url: `/image/${ids}`
    });
  }

  uploadImages(images) {
    return httpClient({
      method: "post",
      url: `/image/`,
      data: {
        images
      }
    });
  }
}

const image = new Image();

export default image;
