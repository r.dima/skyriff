import httpClient from "./httpClient";
import { getPilotId } from "@/helpers/constants";

class Profile {
  getProfile() {
    return httpClient({
      method: "get",
      url: `/pilot/profile/${getPilotId()}`
    });
  }

  updateProfile(data) {
    return httpClient({
      method: "put",
      url: `/pilot/profile/${getPilotId()}`,
      data: {
        ...data
      }
    });
  }
}

const profile = new Profile();

export default profile;
