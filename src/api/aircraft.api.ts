import httpClient from "./httpClient";
// import { aircraftId } from "@/helpers/constants";
import { getPilotId } from "@/helpers/constants";

class Aircraft {
  getAircraft() {
    return httpClient({
      method: "get",
      url: `/pilot/aircraft/${getPilotId()}`
    });
  }

  updateAircraft(data) {
    return httpClient({
      method: "put",
      url: `/pilot/aircraft/${getPilotId()}`,
      data: {
        ...data
      }
    });
  }
}

const aircraft = new Aircraft();

export default aircraft;
