import httpClient from "./httpClient";

class Questions {
  getQuestions() {
    return httpClient({
      method: "get",
      url: `/pilot/questions/`
    });
  }
}

const questions = new Questions();

export default questions;
