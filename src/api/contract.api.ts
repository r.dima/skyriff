import httpClient from "./httpClient";
import { getPilotId } from "@/helpers/constants";

class Contract {
  getContract() {
    return httpClient({
      method: "get",
      url: `/pilot/contract/${getPilotId()}`
    });
  }

  signContract() {
    return httpClient({
      method: "post",
      url: `/pilot/contract/${getPilotId()}`
    });
  }
}

const contract = new Contract();

export default contract;
