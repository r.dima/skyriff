import axios from "axios";
import store from "../store/store";
import i18n from "@/lang/index";
import router from "@/router.ts";
import { USER_TOKEN } from "@/helpers/constants";
import Cookies from "js-cookie";
// import getLang from '@/helpers/getLang'

const lang = "en"; //getLang()

const httpClient = axios.create({
  baseURL: `${process.env.VUE_APP_API_HOST}/api`,
  headers: {
    "Content-Type": "application/json"
  }
});

// before sent
httpClient.interceptors.request.use(config => {
  const token = Cookies.get(USER_TOKEN);

  if (token != null) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  store.dispatch("appState/setLoading", { isOn: true });
  return config;
});
// response
httpClient.interceptors.response.use(
  response => {
    store.dispatch("appState/setLoading", { isOn: false });

    if (response.status === 200) {
      return response;
    }
    return null;
  },
  err => {
    store.dispatch("appState/setLoading", { isOn: false });

    if (err.response) {
      if (err.response.status === 400) {
        store.dispatch("appState/showAlert", {
          mode: "danger",
          text: i18n.t("notifications.wrongData")
        });
      } else if (err.response.status === 403) {
        store.dispatch("appState/showAlert", {
          mode: "danger",
          title: i18n.t("notifications.failed"),
          text: i18n.t("notifications.wrongEmailOrPass")
        });
      } else if (err.response.status === 401) {
        router.push("/login/");
      } else {
        store.dispatch("appState/showAlert", {
          mode: "danger",
          title: i18n.t("notifications.ohNo")
        });
      }
    }
  }
);

export default httpClient;
