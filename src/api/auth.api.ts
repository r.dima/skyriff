import httpClient from "./httpClient";

class Aircraft {
  login(email, password) {
    return httpClient({
      method: "post",
      url: `/auth/login/`,
      data: {
        email,
        password
      }
    });
  }

  register(email, password, punctualScore, angryScore, responsibleScore) {
    return httpClient({
      method: "post",
      url: `/auth/registration/`,
      data: {
        email,
        password,
        punctualScore,
        angryScore,
        responsibleScore
      }
    });
  }
}

const aircraft = new Aircraft();

export default aircraft;
