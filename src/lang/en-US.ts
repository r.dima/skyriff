export default {
  pilotPanel: {
    bookings: {
      title: "Bookings",
      bookingId: "Booking id",
      departDateFrom: "Departure date - from",
      departDateTo: "Departure date - to"
    },
    transfer: {
      title: "Transfers"
    },
    myAircraft: {
      title: "My aircraft",
      photos: "Photos",
      aircraftName: "Aircraft name and model",
      type: "Type",
      airportName: "Airport name / location",
      passangers: "Passangers",
      maxTime: "Max time in air (minutes)",
      generalInfo: "General information",
      lugguage: "Lugguage",
      bagsUnderTenKg: "Bags up to 10 KG",
      bagsAboveTenKg: "Bags above 10 KG",
      handLuggageOnly: "Hand luggage only",
      maxWidth: "Max width (cm)",
      maxHeight: "Max height (cm)",
      maxDepth: "Max depth (cm)",
      noPhotoWarning:
        "Aircraft without photos will not be shown on the public website"
    },
    profile: {
      title: "Profile",
      photo: "Photo",
      firstSave: "Save and continue",
      generalInfo: "General information",
      fullName: "Full name (English)",
      birthday: "Birthday",
      email: "Email",
      country: "Country",
      city: "City",
      currency: "Currency",
      phone: "Phone",
      phone2: "Phone 2",
      languages: "Languages",
      changePassword: "Password change",
      oldPassword: "Old password",
      newPassword: "New password",
      confirmPassword: "Confirm password",
      pilotLicencePhoto: "Pilot licence photo",
      pilotBankNumber: "Bank number (IBAN)",
      bookingTime: "Booking time",
      allDayLong: "All day long",
      from: "From",
      to: "To",
      canBeBookedBeforeFlight: "Hours before flight when booking is available",
      noPhotoWarning:
        "Profile without photo will not be shown on the public website"
    },
    dropdownMenu: {
      logout: "Logout"
    },
    contract: {
      title: "Contract",
      signedLabel: "You have signed this contract",
      unsignedLabel: "You have not signed this contract yet",
      signedMsg: "You have successfully signed the contract",
      signCheckBox: "I have read and agree to this contract",
      signed: "signed"
    },
    referral: {
      title: "Referral",
      description:
        "Invite your friend with a link below and get a <b>50%</b> discount from our commission for 1 month. It will be applied to both of you. More friends - more months you will pay only half of price. The discount can be activated whenever you want after your friend have a first flight",
      copy: "Copy",
      url: "Your link",
      success: "Successfully",
      copiedToClipBoard: "copied to the clipboard"
    },
    tours: {
      title: "Tours"
    },
    calendar: {
      title: "Calendar"
    },
    income: {
      title: "Income"
    },
    faq: {
      title: "Questions"
    }
  },
  generalTitles: {
    export: "Export",
    records: "Record(s)",
    save: "Save",
    oopsTitle: "Error",
    errOccured: "Please try again",
    cancel: "Cancel",
    remove: "Remove",
    add: "Add",
    agreeBtn: "Yes",
    disagreeBtn: "No",
    mandatoryError: "This field is mandatory",
    maxLengthError: "Maximum length is {symbols} letters",
    minLengthError: "Minimum length is {symbols} letters",
    losingDataWarning: "All inserted data will be lost. Continue?",
    removeWarning: "Are you sure you want to remove ",
    dateError: "This date cannot be larger than today",
    emailError: "This email is incorrect",
    latinError: "Only latin letters and numbers are allowed",
    actions: "Actions",
    georgian: "Georgian",
    english: "English",
    russian: "Russian",
    persian: "Persian",
    listIsEmpty: "List is empty",
    clickToRemove: "Click to remove",
    close: "Close",
    confirm: "Confirm",
    noData: "No data",
    search: "Search",
    filter: "Filter",
    reset: "Reset",
    edit: "Edit",
    upload: "Upload",
    choose: "Choose"
  },
  validationMsg: {
    maxValue: "Must be {val} or less",
    minValue: "Must be {val} or more",
    required: "Required",
    email: "Invalid email",
    min: "Must be at least {val} characters",
    max: "May not be greater than {val} characters",
    numeric: "May only contain numeric characters",
    integer: "Must be an integer",
    decimal: "May be decimal numeric",
    alpha: "Only alphabetic characters allowed",
    alphaDash: "Only alpha-numeric characters, dashes and underscores allowed",
    regex: "Invalid format",
    ip: "Invalid ip address"
  },
  notifications: {
    error: "Error",
    warning: "Warning",
    successfully: "Successfully",
    info: "Info",
    ohNo: "Oh, no",
    failed: "Failed",
    wrongEmailOrPass: "Wrong email or password",
    somethingWentWrong: "Something went wrong",
    wrongData: "Wrong data",
    saved: "Saved"
  },
  public: {
    welcomePilot: {
      join: "Join us now",
      answer: "Submit",
      register: "Register",
      email: "Email",
      password: "Password",
      confimPassword: "Confirm password"
    }
  }
};
