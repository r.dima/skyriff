import { Vue } from "vue-property-decorator";
import VueI18n from 'vue-i18n'
import en from './en-US'

Vue.use(VueI18n)

const messages = {
  en
}

export default new VueI18n({
  locale: 'en', // set default locale
  fallbackLocale: 'en',
  messages: messages
})
