import { Vue } from "vue-property-decorator";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      // =============================================================================
      // MAIN LAYOUT ROUTES
      // =============================================================================
      path: "",
      component: () => import("./layouts/main/Main.vue"),
      children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
        {
          path: "/",
          name: "bookings",
          component: () => import("./views/Bookings.vue")
        },
        {
          path: "/referral/",
          name: "referral",
          component: () => import("./views/Referral.vue")
        },
        {
          path: "/my-aircraft/",
          name: "my-aircraft",
          component: () => import("./views/MyAircraft.vue")
        },
        {
          path: "/profile/",
          name: "profile",
          component: () => import("./views/Profile.vue")
        },
        {
          path: "/contract/",
          name: "contract",
          component: () => import("./views/Contract.vue")
        },
        {
          path: "/faq/",
          name: "faq",
          component: () => import("./views/FAQ.vue")
        }
      ]
    },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
    {
      path: "",
      component: () => import("@/layouts/full-page/FullPage.vue"),
      children: [
        // =============================================================================
        // PAGES
        // =============================================================================
        {
          path: "/login",
          name: "page-login",
          component: () => import("@/views/pages/Login.vue")
        },
        {
          path: "/pages/error-404",
          name: "page-error-404",
          component: () => import("@/views/pages/Error404.vue")
        },
        {
          path: "/welcome-pilot/",
          name: "welcome-pilot",
          component: () => import("@/views/pages/WelcomePilot.vue")
        }
      ]
    },
    // Redirect to 404 page, if no match found
    {
      path: "*",
      redirect: "/pages/error-404"
    }
  ]
});

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById("loading-bg");
  if (appLoading) {
    appLoading.style.display = "none";
  }
});

export default router;
