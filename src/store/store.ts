import { Vue } from "vue-property-decorator";
import Vuex from 'vuex'
import vuexy from '@/store/modules/vuexy/index'
import appState from '@/store/modules/appState/index'
import auth from '@/store/modules/auth/index'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
      vuexy,
      auth,
      appState
    },

    strict: process.env.NODE_ENV !== 'production'
})
