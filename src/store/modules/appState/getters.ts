const getters = {
  getAlertInfo: state => {
    return {
      mode: state.mode,
      title: state.title,
      text: state.text
    }
  }
}

export default getters
