const actions = {
  showAlert({ commit }, { mode = "", title = "", text = "" }) {
    commit("SHOW_ALERT", { mode, title, text });
  },

  resetAlert({ commit }) {
    commit("RESET_ALERT");
  },

  setLoading({ commit }, isOn) {
    commit("SET_LOADING", isOn);
  }
};

export default actions;
