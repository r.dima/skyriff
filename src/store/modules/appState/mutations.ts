import i18n from "@/lang/index";

const mutations = {
  SHOW_ALERT(state, { mode, title, text }) {
    (state.showAlert = true),
      (state.mode = mode || "danger"),
      (state.title = title || i18n.t("notifications.error")),
      (state.text = text || i18n.t("notifications.somethingWentWrong"));
  },

  RESET_ALERT(state) {
    (state.showAlert = false),
      (state.mode = ""),
      (state.title = ""),
      (state.text = "");
  },

  SET_LOADING(state, { isOn }) {
    state.loading = isOn;
  }
};

export default mutations;
