const state = {
  showAlert: false,
  mode: "danger",
  title: "",
  text: "",
  loading: false
};

export default state;
