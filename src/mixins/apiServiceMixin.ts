import { USER_TOKEN } from "@/helpers/constants";
import Cookies from "js-cookie";
import { AUTH_LOGOUT } from "@/helpers/types";

export default {
  computed: {
    authToken() {
      return Cookies.get(USER_TOKEN);
    }
  },
  methods: {
    logOut() {
      // this.$store.dispatch(`auth/${AUTH_LOGOUT}`);
      // this.$router.push(`/${this.$i18n.locale}/`);
    }
  }
};
