import { Component, Vue, Watch } from "vue-property-decorator";

@Component
export default class NotificationMixin extends Vue {
  @Watch("$store.state.appState.showAlert")
  showAlertChange(val) {
    if (val) {
      const data = this.$store.getters["appState/getAlertInfo"];
      // @ts-ignore
      this.$vs.notify({
        time: 3700,
        title: data.title,
        text: data.text,
        color: data.mode
      });

      this.$store.dispatch("appState/resetAlert");
    }
  }
}
