import { apiServiceMixin } from "@/mixins";

export default {
  mixins: [apiServiceMixin],
  methods: {
    async getImageById(id) {
      if (id) {
        const response = await new Promise(resolve => {
          const request = new XMLHttpRequest();
          request.open("GET", `${process.env.API_HOST}/api/image/${id}`, true);
          // @ts-ignore
          request.setRequestHeader("Authorization", this.authToken);
          request.responseType = "arraybuffer";
          request.onload = function(e) {
            // @ts-ignore
            const data = new Uint8Array(e.srcElement.response);
            // @ts-ignore
            const raw = String.fromCharCode.apply(null, data);
            const base64 = btoa(raw);
            resolve("data:image;base64," + base64);
          };

          request.send();
        }).then(function(data) {
          return data;
        });

        return response;
      } else {
        return "";
      }
    }
  }
};
