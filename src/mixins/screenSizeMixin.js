export default {
  data() {
    return {
      screenSize: {
        width: window.innerWidth
      }
    }
  },

  computed: {
    isMobile() {
      return this.screenSize.width < 576
    }
  },

  mounted() {
    window.onresize = () => {
      this.screenSize.width = window.innerWidth
    }
  }
}