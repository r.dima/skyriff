import Cookies from "js-cookie";

export const USER_TOKEN = "user-token";
export const USER_NAME = "user-name";
export const PILOT_ID = "pilot-id";
export const AIRCRAFT_ID = "aircraft-id";
export const FIRST_VISIT = "first-visit";

export function getPilotId() {
  return Cookies.get(PILOT_ID);
}
// export const aircraftId = Cookies.get(AIRCRAFT_ID);

export const AIRCRAFT_TYPES = [
  {
    id: "helicopter",
    name: "Helicopter"
  },
  {
    id: "propeller",
    name: "Propeller plane"
  },
  {
    id: "jet",
    name: "Jet"
  },
  {
    id: "glider",
    name: "Glider"
  },
  {
    id: "balloon",
    name: "Hot air balloon"
  }
];

export const CURRENCIES = [
  {
    id: "AED",
    name: "UAE Dirham"
  },
  {
    id: "QAR",
    name: "Qatari Riyal"
  },
  {
    id: "USD",
    name: "United States Dollar"
  },
  {
    id: "EUR",
    name: "Euro"
  },
  {
    id: "GBP",
    name: "Pound Sterling"
  },
  {
    id: "AUD",
    name: "Australian Dollar"
  },
  {
    id: "CAD",
    name: "Canadian Dollar"
  },
  {
    id: "CHF",
    name: "Swiss Franc"
  },
  {
    id: "DOP",
    name: "Dominican Peso"
  },
  {
    id: "FJD",
    name: "Fiji Dollar"
  },
  {
    id: "BRL",
    name: "Brazilian Real"
  },
  {
    id: "NOK",
    name: "Norwegian Krone"
  },
  {
    id: "MXN",
    name: "Mexican Peso"
  },
  {
    id: "NZD",
    name: "New Zealand Dollar"
  },
  {
    id: "THB",
    name: "Thai Baht"
  },
  {
    id: "SGD",
    name: "Singapore Dollar"
  },
  {
    id: "JPY",
    name: "Japanese Yen"
  },
  {
    id: "SAR",
    name: "Saudi Riyal"
  },
  {
    id: "ZAR",
    name: "South African Rand"
  }
];
